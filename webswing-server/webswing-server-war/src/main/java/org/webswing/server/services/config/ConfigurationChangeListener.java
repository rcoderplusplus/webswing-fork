package org.webswing.server.services.config;
public interface ConfigurationChangeListener {

	void notifyChange(ConfigurationChangeEvent e);
}